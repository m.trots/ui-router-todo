angular.module("app.router", []).config([
  "$stateProvider",
  "$urlRouterProvider",

  function($stateProvider, $urlRouterProvider) {
    "ngInject";
    $urlRouterProvider.when("", "/");
    $urlRouterProvider.otherwise("/");

    $stateProvider
      .state("main", {
        url: "/",
        component: "todoList"
      })
      .state("detail", {
        url: "/detail?id",
        component: "detailTodo"
      });
  }
]);
