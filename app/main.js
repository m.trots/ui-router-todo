angular
  .module("app", [
    "ui.router",
    // "templates",
    "app.service",
    "push.service",
    "app.todolist.component",
    "app.detailtodo.component",
    "app.router"
  ])
  .run(function(PushService) {
    document.addEventListener("deviceready", function() {
      PushService.registartion();
    });
  });
