const detailTodo = {
  templateUrl: "app/components/detail/detail.html",
  controller: class {
    constructor(TodoService, $stateParams) {
      "ngInject";
      this.todoService = TodoService;
      this.$stateParams = $stateParams;
    }

    $onInit() {
      const id = +this.$stateParams.id;
      this.todo = this.todoService.getById(id);
    }
  }
};

angular
  .module("app.detailtodo.component", [])
  .component("detailTodo", detailTodo);
