const TODO_API = "http://10.10.2.96:4000";

class TodoService {
  constructor($http, PushService) {
    "ngInject";
    this.$http = $http;
    this.pushService = PushService;
    this.list = [];
    this.description = "";
    this.todo = {};
    this.time = new Date();
  }

  getAll() {
    this.$http({
      method: "GET",
      url: TODO_API
    }).then(response => {
      this.list = response.data;
    });
  }

  getById(id) {
    return this.list.find(item => item.id === id);
  }

  add(todo) {
    const todoItem = {
      title: todo.titleText,
      done: false,
      description: this.description,
      id: Date.now()
    };

    this.$http({
      method: "POST",
      url: TODO_API,
      data: JSON.stringify(todoItem),
      contentType: "application/json"
    }).then(response => {
      this.list.push(todoItem);
      todo.titleText = "";
      // this.list = response.data;
    });
  }

  edit(todo) {
    const foundIndex = this.list.findIndex(x => x.id == todo.id);
    this.list[foundIndex] = todo;
    todo.registrationId = this.pushService.deviceId;
    this.$http({
      method: "PATCH",
      url: TODO_API,
      data: JSON.stringify(todo),
      contentType: "application/json"
    }).then(response => {
      this.list = response.data;
    });
  }

  remove(index) {
    const itemIndex = { index: index };

    this.$http
      .delete(TODO_API, {
        data: itemIndex,
        headers: { "Content-Type": "application/json;charset=utf-8" }
      })
      .then(response => {
        this.list.splice(index, 1);
      });
  }
}

angular.module("app.service", []).service("TodoService", TodoService);
