class PushService {
  constructor($state) {
    this.deviceId = "";
    this.$state = $state;
  }

  registartion() {
    const push = PushNotification.init({
      android: {}
    });

    push.on("registration", data => {
      console.log(data);
      console.log(data.registrationId);
      this.deviceId = data.registrationId;
    });

    push.on("notification", data => {
      console.log("ddd", data);
      const todois = data.additionalData.identifierNumber;

      this.$state.go("detail", { id: todois });
      // data.message,
      // data.title,
      // data.count,
      // data.sound,
      // data.image,
      // data.additionalData
    });

    push.on("error", e => {
      console.log(e.message);
    });
  }
}
angular.module("push.service", []).service("PushService", PushService);
