const gulp = require('gulp');
const concat = require('gulp-concat');
const ngAnnotate = require('gulp-ng-annotate');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const babel = require('gulp-babel');
let cleanCSS = require('gulp-clean-css');
const del = require('del');
const browserSync = require('browser-sync').create();
const templateCache = require('gulp-angular-templatecache');



const jsFiles = [
	// './index.js',
	'./node_modules/angular/angular.min.js',
	'./node_modules/@uirouter/angularjs/release/angular-ui-router.min.js',
	// './public/templates.js',
	'./app/router.js',
	'./app/services/push.service.js',
	'./app/services/push.service.js',
	'./app/components/todolist/todolist.js',
	'./app/components/detail/detail.js',
	'./app/main.js'
];

function styles() {
	return gulp.src('./styles.css')
		.pipe(cleanCSS({level: 2}))
		.pipe(gulp.dest('./css'))
		.pipe(browserSync.stream());
}

function scripts() {
	return gulp.src(jsFiles)
		.pipe(babel({
			presets: ['@babel/env']
		}))
		.pipe(ngAnnotate({add: true}))
		.pipe(concat('script.js'))
		.pipe(uglify({mangle: true}))
		.pipe(gulp.dest('./js'))
		.pipe(browserSync.stream());
}

function clean() {
	return del(['build/css/*', 'build/js/*'])
}

// function watch() {
// 	browserSync.init({
// 		server: {
// 			baseDir: "./"
// 		}
// 	});
// 	gulp.watch('./styles.css', styles)
// 	gulp.watch(jsFiles, styles)
// 	gulp.watch("./*.html").on('change', browserSync.reload);
// }

gulp.task('styles', styles);
gulp.task('scripts', scripts);
gulp.task('del', clean);

// gulp.task('default', function () {
// 	return gulp.src('app/components/**/*.html')
// 		.pipe(templateCache())
// 		.pipe(gulp.dest('./public'));
// });

// gulp.task('watch', watch);
gulp.task('build', gulp.series(clean, gulp.parallel(styles, scripts)));
// gulp.task('dev', gulp.series('build', 'watch'));

